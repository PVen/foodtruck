import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateFilterFn } from '@angular/material/datepicker';
import { BehaviorSubject, Observable, Subject, catchError, forkJoin, startWith, switchMap, takeUntil, tap } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ReservationService } from 'src/app/core/services/reservation.mock.service';
import { TruckService } from 'src/app/core/services/truck.mock.service';
import { Reservation } from 'src/app/shared/models/reservation.model';
import { Truck } from 'src/app/shared/models/truck.model';

@Component({
  selector: 'app-reservations-new',
  templateUrl: './reservations-new.component.html',
  styleUrls: ['./reservations-new.component.scss'],
})
export class ReservationsNewComponent implements OnInit, OnDestroy {

  public form: FormGroup = this.fb.group({
    name: ['', Validators.required],
    date: ['', Validators.required]
  });
  public loading: boolean = false;

  get nameControl() {
    return this.form.get('name') as FormControl;
  }
  get dateControl() {
    return this.form.get('date') as FormControl;
  }

  public truckList: Truck[] = [];
  public min: Date = new Date();
  private selectedTruck: string = '';
  private reservationList: Reservation[] = [];
  private reservationDates: Date[] = [];

  private reload$ = new BehaviorSubject<null>(null);
  private unsub$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private truckService: TruckService,
    private reservationService: ReservationService,
    private notificationService: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.reload$.pipe(
      tap(() => this.loading = true),
      switchMap(() => forkJoin([
        this.truckList$(),
        this.reservationList$()
      ])),
      tap(() => this.loading = false),
      switchMap(() => this.nameControl.valueChanges.pipe(
        startWith(this.selectedTruck)
      )),
      tap((selectedTruck: string) => {
        this.selectedTruck = selectedTruck;
        if(selectedTruck) {
          this.setReservationDateList(selectedTruck)
        }
      }),
      catchError((err) => {
        this.loading = false;
        throw err;
      }),
      takeUntil(this.unsub$)
    )
    .subscribe({error: (err) => console.error(err)});
  }

  private setReservationDateList(truckName: string) {
    const list: Date[] = this.reservationList.reduce((acc, current) => {
      if (current.name === truckName) {
        acc.push(current.date);
      }
      return acc;
    }, <Date[]>[]);
    this.reservationDates = list;
  }

  private truckList$(): Observable<Truck[]> {
    return this.truckService.getAllTrucks().pipe(
      tap((val) => this.truckList = val)
    );
  }

  private reservationList$(): Observable<Reservation[]> {
    return this.reservationService.getAllReservations().pipe(
      tap((val) => this.reservationList = val)
    );
  }

  public onSave() {
    if (this.form.invalid) return;
    this.loading = true;
    const reservation: Reservation = this.form.getRawValue();
    this.reservationService.addReservation(reservation).pipe(
      tap(() => {
        this.loading = false;
        this.dateControl.reset();
        this.notificationService.success('New Reservation Was Added');
        this.reload$.next(null);
      }),
      catchError((err) => {
        this.loading = false;
        throw err;
      })
    )
    .subscribe({error: (err) => console.error(err)});
  }

  public dateFilter: DateFilterFn<Date|null> = (date: Date|null): boolean => {
    if (!date) return false;
    return !this.reservationDates.find(d => d.getTime() == date.getTime());
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


}
