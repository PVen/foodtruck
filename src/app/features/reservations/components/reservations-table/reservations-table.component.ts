import { Component, OnInit, Self } from '@angular/core';
import { Observable, catchError, tap } from 'rxjs';
import { ReservationService } from 'src/app/core/services/reservation.mock.service';
import { MatTableWrapperDataService } from 'src/app/shared/components/mat-table-wrapper/mat-table-wrapper-data.service';
import { PageTitleButtonConfig } from 'src/app/shared/components/page-title/page-title.model';
import { Reservation } from 'src/app/shared/models/reservation.model';
@Component({
  selector: 'app-reservations-table',
  templateUrl: './reservations-table.component.html',
  styleUrls: ['./reservations-table.component.scss'],
  providers: [MatTableWrapperDataService]
})
export class ReservationsTableComponent implements OnInit {

  public displayedColumns: string[] = ['index', 'name', 'date'];
  public buttonConfig: PageTitleButtonConfig = {
    link: ['./new'],
    text: 'New Reservation'
  };
  public loading: boolean = false;

  private reservationList: Reservation[] = [];

  constructor(
    @Self() private tableDataService: MatTableWrapperDataService<Reservation>,
    private reservationService: ReservationService
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.reservationList$().pipe(
      tap((list) => {
        this.tableDataService.setData(list);
        this.loading = false;
      }),
      catchError((err) => {
        this.tableDataService.setData(null);
        this.loading = false;
        throw err;
      })
    )
    .subscribe({error: (err) => console.error(err)});

  }

  private reservationList$(): Observable<Reservation[]>  {
    return this.reservationService.getAllReservations().pipe(
      tap((val) => this.reservationList = val)
    );
  }

}
