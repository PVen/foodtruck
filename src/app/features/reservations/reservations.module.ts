import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationsRoutingModule } from './reservations-routing.module';
import { ReservationsTableComponent } from './components/reservations-table/reservations-table.component';
import { ReservationsNewComponent } from './components/reservations-new/reservations-new.component';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ReservationsTableComponent,
    ReservationsNewComponent
  ],
  imports: [
    CommonModule,
    ReservationsRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class ReservationsModule { }
