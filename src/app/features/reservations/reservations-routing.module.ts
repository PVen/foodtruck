import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReservationsTableComponent } from './components/reservations-table/reservations-table.component';
import { ReservationsNewComponent } from './components/reservations-new/reservations-new.component';

const routes: Routes = [
  { path: '', component: ReservationsTableComponent },
  { path: 'new', component: ReservationsNewComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ReservationsRoutingModule { }
