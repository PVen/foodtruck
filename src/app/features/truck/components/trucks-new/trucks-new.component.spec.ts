import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrucksNewComponent } from './trucks-new.component';

describe('TruckNewComponent', () => {
  let component: TrucksNewComponent;
  let fixture: ComponentFixture<TrucksNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrucksNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrucksNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
