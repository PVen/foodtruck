import { Component, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl,
  FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, catchError, filter, map, switchMap, take, tap } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { TruckService } from 'src/app/core/services/truck.mock.service';
import { Truck } from 'src/app/shared/models/truck.model';

@Component({
  selector: 'app-trucks-new',
  templateUrl: './trucks-new.component.html',
  styleUrls: ['./trucks-new.component.scss']
})
export class TrucksNewComponent implements OnInit {

  public form: FormGroup = this.fb.group({
    name: ['', [Validators.required], [this.uniquNnameValidator()]],
  });
  public loading: boolean = false;
  public truckNameList$ = new BehaviorSubject<string[]|null>(null);
  private truckList: Truck[] = [];
  private reload$ = new BehaviorSubject<null>(null);

  get nameControl() {
    return this.form.get('name') as FormControl;
  }

  constructor(
    private fb: FormBuilder,
    private truckService: TruckService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.reload$.pipe(
      tap(() => this.loading = true),
      switchMap(() => this.truckList$()),
      tap(() => this.loading = false),
      catchError((err) => {
        this.loading = false;
        throw err;
      })
    )
    .subscribe({error: (err) => console.error(err)});
  }

  private truckList$(): Observable<Truck[]> {
    return this.truckService.getAllTrucks().pipe(
      tap((val) => {
        this.truckList = val;
        this.truckNameList$.next(val.map(t => t.name))
      })
    );
  }

  public onSave() {
    if (this.form.invalid || this.form.pending) return;
    this.loading = true;
    const truck: Truck = this.form.getRawValue();
    this.truckService.addTruck(truck).pipe(
      tap(() => {
        this.loading = false;
        this.form.reset();
        this.reload$.next(null);
        this.notificationService.success('New Food Truck Was Added');
      }),
      catchError((err) => {
        this.loading = false;
        throw err;
      })
    )
    .subscribe({error: (err) => console.error(err)})
  }

  private uniqueName(val: string): Observable<boolean> {
    return this.truckNameList$.pipe(
      filter(Boolean),
      map((list) => list.some((name) => name === val)),
    );
  }

  private uniquNnameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.uniqueName(control.value).pipe(
        take(1),
        map(res => res ? { exists: true } : null)
      );
    };
  }

}
