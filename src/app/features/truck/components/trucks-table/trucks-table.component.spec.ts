import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrucksTableComponent } from './trucks-table.component';

describe('TruckTableComponent', () => {
  let component: TrucksTableComponent;
  let fixture: ComponentFixture<TrucksTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrucksTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrucksTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
