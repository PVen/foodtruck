import { Component, OnInit, Self } from '@angular/core';
import { Observable, catchError, tap } from 'rxjs';
import { TruckService } from 'src/app/core/services/truck.mock.service';
import { MatTableWrapperDataService } from 'src/app/shared/components/mat-table-wrapper/mat-table-wrapper-data.service';
import { PageTitleButtonConfig } from 'src/app/shared/components/page-title/page-title.model';
import { Truck } from 'src/app/shared/models/truck.model';

@Component({
  selector: 'app-trucks-table',
  templateUrl: './trucks-table.component.html',
  styleUrls: ['./trucks-table.component.scss'],
  providers: [MatTableWrapperDataService]
})
export class TrucksTableComponent implements OnInit {

  public displayedColumns: string[] = ['index', 'name'];
  public buttonConfig: PageTitleButtonConfig = {
    link: ['./new'],
    text: 'Add Food Truck'
  };
  public loading: boolean = false;

  private truckList: Truck[] = [];

  constructor(
    @Self() private tableDataService: MatTableWrapperDataService<Truck>,
    private truckService: TruckService
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.truckList$().pipe(
      tap((list) => {
        this.tableDataService.setData(list);
        this.loading = false;
      }),
      catchError((err) => {
        this.tableDataService.setData(null);
        this.loading = false;
        throw err;
      })
    )
    .subscribe({error: (err) => console.error(err)});
  }

  private truckList$(): Observable<Truck[]> {
    return this.truckService.getAllTrucks().pipe(
      tap((val) => this.truckList = val)
    );
  }

}
