import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrucksTableComponent } from './components/trucks-table/trucks-table.component';
import { TrucksNewComponent } from './components/trucks-new/trucks-new.component';
import { TrucksRoutingModule } from './trucks-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';

@NgModule({
  declarations: [
    TrucksTableComponent,
    TrucksNewComponent
  ],
  imports: [
    CommonModule,
    TrucksRoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class TrucksModule { }
