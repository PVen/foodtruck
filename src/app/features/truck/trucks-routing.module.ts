import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TrucksTableComponent } from './components/trucks-table/trucks-table.component';
import { TrucksNewComponent } from './components/trucks-new/trucks-new.component';

const routes: Routes = [
  { path: '', component: TrucksTableComponent },
  { path: 'new', component: TrucksNewComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TrucksRoutingModule { }
