export interface PageTitleButtonConfig {
  text: string;
  link: string[];
}
