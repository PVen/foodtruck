import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { PageTitleButtonConfig } from './page-title.model';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageTitleComponent implements OnInit {

  @Input() title: string = '';
  @Input() buttonConfig: PageTitleButtonConfig|undefined = undefined;
  @Input() spinner: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
