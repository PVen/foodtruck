import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mat-spinner-wrapper',
  templateUrl: './mat-spinner-wrapper.component.html',
  styleUrls: ['./mat-spinner-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatSpinnerWrapperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
