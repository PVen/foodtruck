import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatSpinnerWrapperComponent } from './mat-spinner-wrapper.component';

describe('MatSpinnerWrapperComponent', () => {
  let component: MatSpinnerWrapperComponent;
  let fixture: ComponentFixture<MatSpinnerWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatSpinnerWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatSpinnerWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
