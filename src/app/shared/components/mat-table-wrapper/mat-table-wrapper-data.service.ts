import { Injectable } from '@angular/core';
import { BehaviorSubject, filter } from 'rxjs';

@Injectable()
export class MatTableWrapperDataService<T> {

  private dataSub = new BehaviorSubject<T[]>([]);
  public data$ = this.dataSub.asObservable().pipe(
    filter(Boolean)
  );

  constructor() { }

  public setData(val: T[]|null) {
    this.dataSub.next(val ?? []);
  }
}
