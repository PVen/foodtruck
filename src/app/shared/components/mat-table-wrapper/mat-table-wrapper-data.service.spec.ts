import { TestBed } from '@angular/core/testing';

import { MatTableWrapperDataService } from './mat-table-wrapper-data.service';

describe('MatTableWrapperDataService', () => {
  let service: MatTableWrapperDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatTableWrapperDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
