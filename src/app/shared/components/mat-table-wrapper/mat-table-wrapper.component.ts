import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTableWrapperDataService } from './mat-table-wrapper-data.service';
import { Observable, map, tap } from 'rxjs';
import { CdkTableDataSourceInput } from '@angular/cdk/table';

@Component({
  selector: 'app-mat-table-wrapper',
  templateUrl: './mat-table-wrapper.component.html',
  styleUrls: ['./mat-table-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatTableWrapperComponent<T> implements OnInit {

  public dataSource = new MatTableDataSource<T>([]);
  public data$: Observable<CdkTableDataSourceInput<T>> = this.tableDataService.data$
    .pipe(
      tap((val) => this.dataSource.data = val),
      map(() => this.dataSource)
    );

  @Input() displayedColumns: string[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private tableDataService: MatTableWrapperDataService<T>
  ) { }

  ngOnInit(): void {}

  ngAfterViewInit() {
    if (this.paginator && this.sort) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

}
