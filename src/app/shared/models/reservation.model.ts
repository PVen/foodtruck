export interface Reservation {
  name: string;
  date: Date;
}
