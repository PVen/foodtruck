import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableWrapperComponent } from './components/mat-table-wrapper/mat-table-wrapper.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { MatSpinnerWrapperComponent } from './components/mat-spinner-wrapper/mat-spinner-wrapper.component';
@NgModule({
  declarations: [
    PageTitleComponent,
    MatTableWrapperComponent,
    MatSpinnerWrapperComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    PageTitleComponent,
    MatTableWrapperComponent,
    RouterModule,
    ////
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SharedModule { }
