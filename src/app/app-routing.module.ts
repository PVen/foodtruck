import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: '/reservations', pathMatch: 'full'},
  {path: 'reservations', loadChildren: () => import('./features/reservations/reservations.module').then(m => m.ReservationsModule)},
  {path: 'trucks', loadChildren: () => import('./features/truck/trucks.module').then(m => m.TrucksModule)},
  // {path: '**', component: undefined}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
