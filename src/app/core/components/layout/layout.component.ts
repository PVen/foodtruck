import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Observable, distinctUntilChanged, map } from 'rxjs';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  public mode$: Observable<MatDrawerMode>  = this.breakpointObserver
    .observe([ Breakpoints.Small, Breakpoints.XSmall])
    .pipe(
      map((value) => value.matches ? 'over' : 'side'),
      distinctUntilChanged()
    );

  constructor(
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit(): void {}

}
