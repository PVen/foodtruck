import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, delay, of } from 'rxjs';
import { Reservation } from 'src/app/shared/models/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private mockReservations: Reservation[] = [
    {name: 'Pizza from Mars', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'The Drunken Taco', date: new Date('2023-10-26T00:00:00+02:00') },
    {name: 'Sammie\'s', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Un·cooked', date: new Date('2023-10-26T00:00:00+02:00')},
    {name: 'Water Pig BBQ', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Fry the Coop', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Odd Duck', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Ltd Edition Sushi', date: new Date('2023-11-25T00:00:00+02:00')},
    {name: 'The Catbird Seat', date: new Date('2023-11-25T00:00:00+02:00')},
    {name: 'Obed and Isaac\'s', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Plan B Burger', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Old Fashion Hamburgers', date: new Date('2023-10-28T00:00:00+02:00')},
    {name: 'Holy Smokin BBQ', date: new Date('2023-10-29T00:00:00+02:00')},
    {name: 'Tandem Coffee + Bakery ', date: new Date('2023-10-25T00:00:00+02:00')},
    {name: 'Mr. & Mrs. Bun', date: new Date('2023-10-25T00:00:00+02:00')}
  ];
  private mockRequestDelay: number = 1000;

  private mockReservationsSub = new BehaviorSubject<Reservation[]>(this.mockReservations);

  constructor() { }

  public getAllReservations(): Observable<Reservation[]> {
    return of(this.mockReservationsSub.value).pipe(
      delay(this.mockRequestDelay)
    );
  }

  public addReservation(reservation: Reservation): Observable<Reservation> {
    const reservationsList: Reservation[] = [...this.mockReservationsSub.value, reservation];
    this.mockReservationsSub.next(reservationsList);
    return of(reservation).pipe(
      delay(this.mockRequestDelay)
    );
  }
}
