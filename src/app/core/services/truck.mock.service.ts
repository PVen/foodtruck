import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, delay, of } from 'rxjs';
import { Truck } from 'src/app/shared/models/truck.model';

@Injectable({
  providedIn: 'root'
})
export class TruckService {

  constructor() { }

  private mockTrucks: Truck[] = [
    {name: 'name'},
    {name: 'Pizza from Mars'},
    {name: 'The Drunken Taco'},
    {name: 'Sammie\'s'},
    {name: 'Un·cooked'},
    {name: 'Water Pig BBQ'},
    {name: 'Fry the Coop'},
    {name: 'Odd Duck'},
    {name: 'Ltd Edition Sushi'},
    {name: 'The Catbird Seat'},
    {name: 'Obed and Isaac\'s'},
    {name: 'Plan B Burger'},
    {name: 'Old Fashion Hamburgers'},
    {name: 'Holy Smokin BBQ'},
    {name: 'Tandem Coffee + Bakery '},
    {name: 'Mr. & Mrs. Bun'}
  ];
  private mockRequestDelay: number = 1000;

  private mockTrucksSub = new BehaviorSubject<Truck[]>(this.mockTrucks);

  public getAllTrucks(): Observable<Truck[]> {
    return of(this.mockTrucksSub.value).pipe(
      delay(this.mockRequestDelay)
    );
  }

  public addTruck(truck: Truck): Observable<Truck> {
    const truckList: Truck[] = [...this.mockTrucksSub.value, truck];
    this.mockTrucksSub.next(truckList);
    return of(truck).pipe(
      delay(this.mockRequestDelay)
    );
  }
}
